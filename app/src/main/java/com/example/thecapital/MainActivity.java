package com.example.thecapital;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private TextView nombre;
    private Button buscar;
    private TextView ingreTXT;
    private TextView txt_nutri;
    private TextView nombre_rec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        this.nombre = (EditText) findViewById(R.id.txtIngrediente);
        this.nombre_rec = (TextView) findViewById(R.id.nombre_recipe);
        this.ingreTXT=(TextView) findViewById(R.id.ingreTXT);
        this.buscar=(Button) findViewById(R.id.BtBuscar);
        this.txt_nutri=(TextView) findViewById(R.id.txt_nutri);


        buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String ingrenombre = nombre.getText().toString().trim();

        String url = "https://api.edamam.com/search?q="+ingrenombre+"&app_id=b10681e9&app_key=e088d19db390db24c10ae87d959a7fa6";
        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);


                            JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                            JSONObject primer = hitsJSON.getJSONObject(0);
                            JSONObject recipesJSON  = primer.getJSONObject("recipe");
                            JSONArray in = recipesJSON.getJSONArray("ingredientLines");
                            String nombre =  recipesJSON.getString("label");
                            String ingre = "" ;

                            for(int i = 0; i<in.length();i++){

                                ingre = ingre + in.getString(i)+ "\n" ;


                            }
                            ingreTXT.setText(ingre);
                            nombre_rec.setText(nombre);


                            JSONObject nutJSON = recipesJSON.getJSONObject("totalNutrients");
                            JSONObject enerJSON =nutJSON.getJSONObject("ENERC_KCAL");
                            String enerlabel = enerJSON.getString("label");
                            String enerq = enerJSON.getString("quantity");
                            String eneru = enerJSON.getString("unit");

                            JSONObject fatJSON =nutJSON.getJSONObject("FAT");
                            String fatlabel =fatJSON.getString("label");
                            String fatq = fatJSON.getString("quantity");
                            String fatu = fatJSON.getString("unit");

                            JSONObject fasatJSON =nutJSON.getJSONObject("FASAT");
                            String fasatlabel = fasatJSON.getString("label");
                            String fasatq = fasatJSON.getString("quantity");
                            String fasatu = fasatJSON.getString("unit");


                            String todo = enerlabel + " = " + enerq + "  " + eneru + "\n" +
                                    fatlabel + " = " + fatq + "  " + fatu + "\n" +
                                    fasatlabel + " = " + fasatq + "   " + fasatu + "\n" ;

                            txt_nutri.setText(todo);







                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);

            }
        });
    }



}
